pub fn qsort <T: std::fmt::Debug, F: Fn(&T, &T) -> bool + Copy> (seq: &mut [T], cmp: F) {
    let len = seq.len();
    if len <= 1 {return}
    if len == 2 {
        if !cmp(&seq[0], &seq[1]) {
            seq.swap(0, 1)
        }
        return
    }
    let mut i = 0;
    let mut j = len - 1;
    let pivot = j;
    loop {
        while i < j && cmp(&seq[i], &seq[pivot]) {i += 1}
        while i < j && cmp(&seq[pivot], &seq[j]) {j -= 1}
        if i < j {seq.swap(i, j)}
        else {break}
    }
    seq.swap(pivot, j);
    qsort(&mut seq[0..j], cmp);
    qsort(&mut seq[j+1..len], cmp);
}