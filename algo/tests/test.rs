extern crate algo;

use algo::qsort::qsort;
#[test]
fn qsort_test() {
    let mut vec = vec![2, 1];
    qsort(&mut vec, |a: &i32, b: &i32| { a <= b });
    assert_eq!(vec, [1, 2]);
    vec = vec![3, 1, 4, 6, 6, 6, 3, 2, 1];
    qsort(&mut vec, |a: &i32, b: &i32| { a <= b });
    assert_eq!(vec, [1, 1, 2, 3, 3, 4, 6, 6, 6]);
    let mut vec = vec!(String::from("aaa"), String::from("bbb"), String::from("ccc"));
    qsort(&mut vec, |a: &String, b: &String| { a >= b });
    assert_eq!(vec, ["ccc", "bbb", "aaa"]);
}