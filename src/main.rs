use structures::vector::Vector;



fn main() {
    println!("Unsafe tests");
    let mut vec: Vector<i32> = Vector::new();
    unsafe {
        vec.push_back(4);
        vec.push_back(5);
        assert_eq!(vec.size(), 2);
        assert_eq!(vec.index(0), &4);
        assert_eq!(vec.index(1), &5);
        let slice = [4,5];
        assert_eq!(vec.equal(&slice), true);
        let x = vec.pop_back();
        assert_eq!(vec.equal(&slice), false);
        assert_eq!(vec.size(), 1);
        assert_eq!(x, 5);
        let std_vec = vec![1,2,3,4,5];
        let my_vec = Vector::from(std_vec);
        assert_eq!(my_vec.equal(&[1,2,3,4,5]), true);
    }
}
