use std::alloc::{Layout, realloc};
use std::cmp::min;
use std::ops::{Deref, Index, IndexMut};
use std::ptr::{null_mut, read};

pub struct Vector <T> {
    head: * mut T,
    len: usize,
    capacity: usize
}

impl <T> Vector <T> where T: PartialEq<T> {
    pub fn size(&self) -> usize {
        self.len
    }

    pub unsafe fn push_back(&mut self, el: T) {
        if self.capacity == 0 {
            self.capacity = 1;
            self.len = 1;
            let el_ptr = Box::new(el);
            self.head = Box::into_raw(el_ptr);
            return;
        }
        if self.len == self.capacity {
            let new_capacity = self.capacity * 2;
            let layout = Layout::array::<T>(self.capacity).unwrap(); //TODO: add safe unwrapping
            let new_head = realloc(self.head as *mut u8, layout, new_capacity);
            self.head = new_head as *mut T;
        }
        let ptr = self.head.add(self.len);
        std::ptr::write(ptr, el);
        self.len += 1;
    }

    pub fn new() -> Self {
        return Vector { head: null_mut(), len: 0, capacity: 0 }
    }

    pub unsafe fn index(&self, index: usize) -> &T {
        if index >= self.len {
            panic!("Index out of range");
        }
        let ptr = self.head.add(index);
        return &*ptr;
    }

    pub unsafe fn equal (&self, arr: &[T]) -> bool {
        if self.size() != arr.len() {
            return false;
        }
        for i in 0..min(arr.len(), self.size()) {
            if &arr[i] != self.index(i) {
                return false;
            }
        }
        return true;
    }

    pub unsafe fn pop_back(&mut self) -> T {
        self.len -= 1;
        let mut ptr = self.head.add(self.len);
        let val = read(ptr);
        if self.len < self.capacity / 2 {
            let new_capacity = self.capacity / 2;
            let layout = Layout::array::<T>(self.capacity).unwrap(); //TODO: add safe unwrapping
            let new_head = realloc(self.head as *mut u8, layout, new_capacity);
            self.head = new_head as *mut T;
        }
        return val;
    }
    pub unsafe fn from(items: Vec<T>) -> Self {
        let mut vec1 = Vector::new();
        for el in items {
            vec1.push_back(el);
        }
        return vec1;
    }
}

#[test]
fn check_len() {
    let vec: Vector<i32> = Vector::new();
    assert_eq!(vec.len, 0);
}
